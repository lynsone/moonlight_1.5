﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoonLight
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            //if the form is minimized  
            //hide it from the task bar  
            //and show the system tray icon (represented by the NotifyIcon control)  
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                Moonlight.Visible = true;
            }
        }
        private void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Play
            Client.Run();
        }

        private void registerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Register
            RegisterForm registerform = new RegisterForm();
            registerform.Show();

        }

        private void voteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Vote
            System.Diagnostics.Process.Start(Client.sWebsiteURL);
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //Exit
            Application.Exit();
        }

        private void Moonlight_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void playBtn_Click(object sender, EventArgs e)
        {
            //Play
            Client.Run();
            WindowState = FormWindowState.Minimized;
        }

        private void registerBtn_Click(object sender, EventArgs e)
        {
            //Register
            RegisterForm registerform = new RegisterForm();
            registerform.Show();
        }
    }
    
}
