﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace MoonLight
{


    public partial class RegisterForm : Form
    {
        

        public RegisterForm()
        {
            

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            MySqlCommand command = new MySqlCommand($"INSERT INTO users (name, password, email) VALUES(@user, @pass, @email)", db.getConnection());

            command.Parameters.Add("@user", MySqlDbType.VarChar).Value = tbUser.Text;
            command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = tbPass.Text;
            command.Parameters.Add("@email", MySqlDbType.VarChar).Value = tbEmail.Text;

            db.openConnection();

            if (!checkTextBoxValues())
            {
                if(tbUser.Text.Length >= 4 || tbPass.Text.Length >= 4) //check if username and password have at least 4 characters
                {
                    if (checkUsername()) //bool returns TRUE so username exists
                    {
                        MessageBox.Show("Username already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else //bool returns FALSE so username doesnt exist
                    {
                        if (command.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Account Successfully Created.", "Account Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("AN ERROR HAS OCCURED","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
               
                }
                else //returns this if username or password are not at least 4 characters long
                {
                    MessageBox.Show("Ensure Username and Password are at least 4 characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                db.closeConnection();
            }
            else //returns this if not all fields are filled up
            {
                MessageBox.Show("Please fill up all fields.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
        public Boolean checkUsername() //function to check if username exists in the database
        {
            DB db = new DB();

            String username = tbUser.Text;

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command = new MySqlCommand($"SELECT * FROM users WHERE name = @usn", db.getConnection());

            command.Parameters.Add("@usn", MySqlDbType.VarChar).Value = username;

            adapter.SelectCommand = command;

            adapter.Fill(table);

            // check if this username already exists in the database
            if (table.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean checkTextBoxValues()
        {
            string user = tbUser.Text;
            string pass = tbPass.Text;
            string email = tbEmail.Text;

            if (user.Equals("") || pass.Equals("") || email.Equals(""))
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }


    }
}
