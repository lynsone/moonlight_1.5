# Moonlight 1.5
Adding functionality to Bluntest's Moonlight for v176

Full credits to his original code Ragezone: http://forum.ragezone.com/f921/v176-client-bypass-1164436/

## Things you will need
**Install Connector/NET:** https://dev.mysql.com/downloads/connector/net/

**Install Visual Studio Community:** https://visualstudio.microsoft.com/vs/community/ 

**v176 localhost setup + Moonlight.dll:** http://www.mediafire.com/file/q8klqzq6r7hcdj4/v176_Localhost_Setup.zip/file

A database (e.g. on MySQL) & a connection to that database (e.g. WAMP)

## How to use
Clone resository and open MoonLight.sln

In Client.cs, edit this line:

```
private MySqlConnection connection = new MySqlConnection("server=localhost;port=3306;username=root;password=;database=swordie");
```

to match your IP and database credentials.

Change from Debug to Release.

Build solution.

Drag Moonlight.exe from Bin/Release folder + all files extracted from v176 Localhost setup into your MapleStory folder. Delete the BlackCipher folder if you have not already done so.

Double-click Moonlight.exe & enjoy!


## Note
My first "release" of any kind and I pretty much hacked everything together using Youtube and Google so please forgive the bad code and teach me how I can improve it. Have fun!
